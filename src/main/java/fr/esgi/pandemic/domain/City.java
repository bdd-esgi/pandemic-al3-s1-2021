package fr.esgi.pandemic.domain;

public class City {
    private final String cityName;
    private int infectionLevel;

    public City(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public int getInfectionLevel() {
        return infectionLevel;
    }

    public void setInfectionLevel(int infectionLevel) {
        this.infectionLevel = infectionLevel;
    }

    public void infect() {
        if (infectionLevel < 3) {
            infectionLevel++;
        }
    }
}
