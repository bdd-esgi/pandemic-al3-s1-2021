package fr.esgi.pandemic.domain;

import java.util.Objects;

public record CityLink(String cityName1, String cityName2) {

    public boolean contains(String name) {
        return Objects.equals(cityName1, name) || Objects.equals(cityName2, name);
    }

    public String other(String name) {
        if (Objects.equals(name, cityName1)) {
            return cityName2;
        } else if (Objects.equals(name, cityName2)) {
            return cityName1;
        }
        throw new IllegalArgumentException(name + " not existing in " + this);
    }

    @Override
    public String toString() {
        return cityName1 + " <---->" + cityName2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityLink cityLink = (CityLink) o;
        return Objects.equals(cityName1, cityLink.cityName1) && Objects.equals(cityName2, cityLink.cityName2)
                || Objects.equals(cityName1, cityLink.cityName2) && Objects.equals(cityName2, cityLink.cityName1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName1, cityName2);
    }
}
