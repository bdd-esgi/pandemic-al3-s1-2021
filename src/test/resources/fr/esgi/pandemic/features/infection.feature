@Occident
Feature: Infection of a city

  Scenario: First infection of a city
      Given Paris has not been infected
      When Paris is infected
      Then Paris' infection level should be 1

  Scenario: Second infection of a city
      Given Paris has been infected 1 time
      When Paris is infected
      Then Paris' infection level should be 2

  Scenario: Third infection of a city
      Given Paris has been infected 2 times
      When Paris is infected
      Then Paris' infection level should be 3

  Scenario: Fourth infection of a city
      Given Paris has been infected 3 times
      When Paris is infected
      Then Paris' infection level should remain 3