package fr.esgi.pandemic.domain;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CityLinkTest {

    @Test
    public void should_be_bidirectional() {
        Assertions.assertThat(new CityLink("Paris", "London")).isEqualTo(new CityLink("London", "Paris"));
    }

    @Test
    public void should_expose_other_city() {
        CityLink cityLink = new CityLink("Paris", "London");
        Assertions.assertThat(cityLink.other("Paris")).isEqualTo("London");
        Assertions.assertThat(cityLink.other("London")).isEqualTo("Paris");
    }

    @Test
    public void should_verify_link_contains_a_city() {
        CityLink cityLink = new CityLink("Paris", "London");
        Assertions.assertThat(cityLink.contains("Paris")).isTrue();
        Assertions.assertThat(cityLink.contains("London")).isTrue();
    }
}