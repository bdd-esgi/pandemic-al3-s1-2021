package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.City;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class InfectionSteps {

    private ScenarioContext context;

    public InfectionSteps(ScenarioContext context) {
        this.context = context;
    }

    @ParameterType(".*")
    public City city(String cityName){
        return context.cityMap.get(cityName);
    }

    @Given("{city} has not been infected")
    public void cityHasNotBeenInfected(City city) {
        city.setInfectionLevel(0);
    }

    @Given("{city} has been infected {int} time(s)")
    public void parisHasBeenInfectedTime(City city, int infectionLevel) {
        city.setInfectionLevel(infectionLevel);
    }

    @When("{city} is infected")
    public void cityIsInfected(City city) {
        city.infect();
    }

    @Then("{city}' infection level should be/remain {int}")
    public void parisInfectionLevelShouldBe(City city, int infectionLevel) {
        Assertions.assertThat(city.getInfectionLevel()).isEqualTo(infectionLevel);
    }
}
