package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.City;
import fr.esgi.pandemic.domain.CityLink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScenarioContext {
    public Map<String, City> cityMap = new HashMap<>();
    public List<CityLink> cityLinks = new ArrayList<>();
}
