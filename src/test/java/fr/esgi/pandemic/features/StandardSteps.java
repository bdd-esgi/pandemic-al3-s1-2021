package fr.esgi.pandemic.features;

import fr.esgi.pandemic.domain.City;
import fr.esgi.pandemic.domain.CityLink;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;

public class StandardSteps {
    private ScenarioContext context;

    public StandardSteps(ScenarioContext context) {
        this.context = context;
    }

    @ParameterType(".*")
    public List<City> cities(String cityNames) {
        return Arrays.stream(cityNames.split(",")).map(cityName -> context.cityMap.get(cityName.trim())).toList();
    }

    @Given("the occident network")
    @Before("@Occident")
    public void theOccidentNetwork() {
        context.cityMap.put("Paris", new City("Paris"));
        context.cityMap.put("London", new City("London"));
        context.cityMap.put("Essen", new City("Essen"));
        context.cityMap.put("Madrid", new City("Madrid"));
        context.cityMap.put("Algiers", new City("Algiers"));
        context.cityMap.put("Milan", new City("Milan"));
        context.cityMap.put("New_york", new City("New_york"));

        context.cityLinks.add(new CityLink("Paris", "London"));
        context.cityLinks.add(new CityLink("Paris", "Essen"));
        context.cityLinks.add(new CityLink("Paris", "Milan"));
        context.cityLinks.add(new CityLink("Paris", "Algiers"));
        context.cityLinks.add(new CityLink("Paris", "Madrid"));

        context.cityLinks.add(new CityLink("Essen", "London"));
        context.cityLinks.add(new CityLink("Essen", "Milan"));

        context.cityLinks.add(new CityLink("Madrid", "London"));
        context.cityLinks.add(new CityLink("Madrid", "Algiers"));
    }

    @Then("the cities should have the following infection levels:")
    public void theCitiesShouldHaveTheFollowingInfectionLevels(DataTable dataTable) {
        dataTable.asMaps().forEach(dataLine -> {
            String cityName = dataLine.get("cityName");
            Integer infectionLevel = Integer.parseInt(dataLine.get("infection level"));
            Assertions.assertThat(context.cityMap.get(cityName).getInfectionLevel()).isEqualTo(infectionLevel);
        });
    }

    @And("{city} should be linked to {cities}")
    public void parisShouldBeLinkedToLinkedCities(City city, List<City> expectedLinkedCities) {
        List<String> expectedLinkedCityNames = expectedLinkedCities.stream().map(City::getCityName).toList();
        List<String> linkedCityNames = context.cityLinks.stream().filter(cityLink -> cityLink.contains(city.getCityName())).map(cityLink -> cityLink.other(city.getCityName())).toList();

        Assertions.assertThat(linkedCityNames).containsExactlyInAnyOrderElementsOf(expectedLinkedCityNames);
    }

}
